import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.poifs.crypt.dsig.SignatureConfig;
import org.apache.poi.poifs.crypt.dsig.SignatureInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8DecryptorProviderBuilder;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;
import org.bouncycastle.pkcs.PKCSException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;

public class SimpleSignV2 {
    public void sign() throws IOException,
            CertificateException
           ,XMLSignatureException, MarshalException, InvalidFormatException {

        String path = "E:/digital-sign/mycert/test.crt";
        char[] pass = "123456".toCharArray();
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        X509Certificate x509Cert = null;
        try (FileReader fileReader = new FileReader(path)) {
            try (PEMParser pemParser = new PEMParser(fileReader)) {
                Object object = null;
                while ((object = pemParser.readObject()) != null) {
                    if (object instanceof X509CertificateHolder) {
                        x509Cert = (X509Certificate) new JcaX509CertificateConverter()
                                .getCertificate((X509CertificateHolder) object);
                        break;
                    }
                }
            }
        }
        java.security.cert.Certificate[] chain = new Certificate[]{x509Cert};
        PrivateKey pk = null;
        try {
            pk = getPrivateKey("E:/digital-sign/mycert/test.key");
        } catch (OperatorCreationException | PKCSException e) {
            e.printStackTrace();
        }
        // filling the SignatureConfig entries (minimum fields, more options are available ...)
        SignatureConfig signatureConfig = new SignatureConfig();
        signatureConfig.setKey(pk);
//        signatureConfig.setDigestAlgo(HashAlgorithm.sha512);
        signatureConfig.setSigningCertificateChain(Collections.singletonList(x509Cert));
        // adding the signature document to the package
        SignatureInfo si = new SignatureInfo();
        OPCPackage pkg = OPCPackage.open("src/main/resources/sample.xlsx", PackageAccess.READ_WRITE);
        si.setOpcPackage(pkg);
        si.setSignatureConfig(signatureConfig);
        si.confirmSignature();

        // optionally verify the generated signature
        boolean b = si.verifySignature();
        assert (b);
        System.out.println(b);
        // write the changes back to disc
        pkg.close();
    }

    public static void main(String[] args) {
        SimpleSignV2 app = new SimpleSignV2();
        try {
            app.sign();
        } catch (IOException | CertificateException | XMLSignatureException |
                MarshalException | InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    public static PrivateKey getPrivateKey(String keyFile)
            throws OperatorCreationException, PKCSException, IOException {
        PrivateKey privateKey = null;
        String passphrase = "123456";
        JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
        try (FileReader fileReader = new FileReader(keyFile)) {
            try (PEMParser pemParser = new PEMParser(fileReader)) {
                Object object = null;
                while ((object = pemParser.readObject()) != null) {
                    if (object instanceof PEMEncryptedKeyPair) {
                        if (passphrase == null)
                            throw new IllegalArgumentException("Need passphrase");
                        PEMDecryptorProvider decProv =
                                new JcePEMDecryptorProviderBuilder()
                                        .build(passphrase.toCharArray());
                        var pair = converter.getKeyPair(((PEMEncryptedKeyPair) object)
                                .decryptKeyPair(decProv));
                        privateKey = pair.getPrivate();
                        break;
                    } else if (object instanceof PKCS8EncryptedPrivateKeyInfo) {
                        if (passphrase == null)
                            throw new IllegalArgumentException("Need passphrase");
                        InputDecryptorProvider decryptionProv =
                                new JceOpenSSLPKCS8DecryptorProviderBuilder()
                                        .build(passphrase.toCharArray());
                        privateKey = converter.getPrivateKey(((PKCS8EncryptedPrivateKeyInfo) object)
                                .decryptPrivateKeyInfo(decryptionProv));
                        break;
                    } else if (object instanceof PEMKeyPair) {
                        var pair = converter.getKeyPair((PEMKeyPair) object);
                        privateKey = pair.getPrivate();
                        break;
                    }
                }
            }
        }
        return privateKey;
    }
}
