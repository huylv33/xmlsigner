import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.poifs.crypt.dsig.SignatureConfig;
import org.apache.poi.poifs.crypt.dsig.SignatureInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import java.awt.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.List;

public class SimpleSign {

    public static void sign()
            throws IOException, GeneralSecurityException, XMLSignatureException, MarshalException, InvalidFormatException {
//        Properties properties = new Properties();
//        properties.load(new FileInputStream("e:/key.properties"));
        String path = "E:/digital-sign/mycert3/test.pfx";
        char[] pass = "123456".toCharArray();
        BouncyCastleProvider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        KeyStore ks = KeyStore.getInstance("pkcs12", provider.getName());
        ks.load(new FileInputStream(path), pass);
        String alias = (String) ks.aliases().nextElement();
        PrivateKey pk = (PrivateKey) ks.getKey(alias, pass);
        Certificate[] chain = ks.getCertificateChain(alias);
        X509Certificate x509 = (X509Certificate)ks.getCertificate(alias);

        for (int i = 0; i < chain.length; i++) {
            X509Certificate cert = (X509Certificate)chain[i];
            System.out.println(String.format("[%s] %s", i, cert.getSubjectDN()));
        }
        SimpleSign app = new SimpleSign();
        SignatureConfig signatureConfig = new SignatureConfig();
        signatureConfig.setKey(pk);
        signatureConfig.setSigningCertificateChain(Collections.singletonList(x509));
        // adding the signature document to the package

        SignatureInfo si = new SignatureInfo();
        OPCPackage pkg = OPCPackage.open("src/main/resources/sample.xlsx", PackageAccess.READ_WRITE);
        si.setOpcPackage(pkg);
        si.setSignatureConfig(signatureConfig);
        si.confirmSignature();

        // optionally verify the generated signature
        boolean b = si.verifySignature();
        assert (b);
        System.out.println(b);
        // write the changes back to disc
        pkg.close();
    }

    public static void main(String[] args) {
        SimpleSign app = new SimpleSign();
        try {
            app.sign();
        } catch (IOException | XMLSignatureException |
                MarshalException | InvalidFormatException | GeneralSecurityException e) {
            e.printStackTrace();
        }
    }
}
